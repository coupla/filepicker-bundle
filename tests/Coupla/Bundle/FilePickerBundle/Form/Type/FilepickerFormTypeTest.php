<?php
/**
 * Copyright (c) 2013 Adam L. Englander
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace Coupla\Bundle\FilePickerBundle\Form\Type;

use Phake;
use Symfony\Component\Form\Form;

/**
 * @author Adam L. Englander <adam.l.englander@coupla.co>
 *
 * Unit tests for the Coupla file picker pick form type
 */
class FilepickerFormTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FilepickerFormType
     */
    private $type;

    /**
     * @var \Symfony\Component\OptionsResolver\OptionsResolverInterface
     * @Mock
     */
    private $resolver;

    /**
     * @var \Symfony\Component\Form\FormView
     * @Mock
     */
    private $formView;

    /**
     * @var string
     */
    private $apiKey;

    protected function setUp()
    {
        Phake::initAnnotations($this);
        $this->apiKey = "API key value";
        $this->type = new FilepickerFormType($this->apiKey);
    }

    public function testGetName()
    {
        $this->assertEquals(
            'filepicker',
            $this->type->getName()
        );
    }

    public function testGetParent()
    {
        $this->assertEquals(
            'text',
            $this->type->getParent()
        );
    }

    public function testSetDefaultOptions()
    {
        $expected = array(
            'data-fp-apikey' => $this->apiKey,
            'data-fp-services' => implode(',', array(
                'BOX',
                'COMPUTER',
                'DROPBOX',
                'EVERNOTE',
                'FACEBOOK',
                'FLICKR',
                'FTP',
                'GITHUB',
                'GOOGLE_DRIVE',
                'PICASA',
                'WEBDAV',
                'GMAIL',
                'IMAGE_SEARCH',
                'INSTAGRAM',
                'URL',
                'VIDEO',
                'WEBCAM'
            ))
        );

        $this->type->setDefaultOptions($this->resolver);
        $actual = null;
        Phake::verify($this->resolver)
            ->setDefaults(Phake::capture($actual));
        $this->assertEquals($expected, $actual);
    }

    public function dataNonFilePickerOptions()
    {
        return array(
            'value'    => array('value'),
            'label'    => array('label'),
            'required' => array('required'),
        );
    }

    /**
     * @dataProvider dataNonFilePickerOptions
     */
    public function testBuildViewDoesNotSetAttributeForNonFilePickerOption($option)
    {
        $form = new Form(Phake::mock('\Symfony\Component\Form\FormConfigInterface'));
        $this->type->buildView(
            $this->formView,
            $form,
            array($option => 'not an attr')
        );
        Phake::verify($this->formView, Phake::never())->setAttribute($option, $this->anything());
    }

    public function dataFilePickerOptions()
    {
        return array(
            'data-fp-apikey'   => array('data-fp-apikey'),
            'data-fp-button-text' => array('data-fp-button-text'),
            'data-fp-button-class' => array('data-fp-button-class'),
            'data-fp-mimetype' => array('data-fp-mimetype'),
            'data-fp-mimetypes' => array('data-fp-mimetypes'),
            'data-fp-extension' => array('data-fp-extension'),
            'data-fp-extensions' => array('data-fp-extensions'),
            'data-fp-container' => array('data-fp-container'),
            'data-fp-store-location' => array('data-fp-store-location'),
            'data-fp-multiple' => array('data-fp-multiple'),
            'data-fp-openTo' => array('data-fp-openTo'),
            'data-fp-service' => array('data-fp-service'),
            'data-fp-services' => array('data-fp-services'),
            'data-fp-maxSize' => array('data-fp-maxSize'),
            'data-fp-debug' => array('data-fp-debug'),
            'data-fp-policy' => array('data-fp-policy'),
            'data-fp-signature' => array('data-fp-signature'),
        );
    }

    /**
     * @dataProvider dataFilePickerOptions
     */
    public function testBuildViewSetsAttributeForFilePickerOption($option)
    {
        $form = new Form(Phake::mock('\Symfony\Component\Form\FormConfigInterface'));
        $this->type->buildView(
            $this->formView,
            $form,
            array($option => 'value')
        );
        Phake::verify($this->formView)
            ->setAttribute($option, 'value');
    }
}
